Android app for Seloc Tech Wiki
=============

This project relies on data that is produced from the [SelocTechWikiParser project](https://bitbucket.org/nzender/seloctechwikiparser/overview).

HOW TO CREATE AN OBB AND PUT IT ON DEVICE FOR TESTING
-------------

1. cd <SelocTechWikiAndroidLocation>/expansionFiles
2. <android-sdk>/tools/jobb -d ./root -v -pn com.seloc.techwiki -pv <version-code> -o main.<version-code>.com.seloc.techwiki.obb
    * <version-code> can be found in AppConstants.groovy
3. chmod 644 *.obb
4. <android-sdk>/platform-tools/adb push main.<version-code>.com.seloc.techwiki.obb /storage/sdcard0/Android/obb/com.seloc.techwiki/main.<version-code>.com.seloc.techwiki.obb

* The newly generated obb file must be added when the APK is uploaded to the Play Store.
