package com.seloc.techwiki;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Messenger;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.ToggleButton;

import com.google.android.vending.expansion.downloader.DownloadProgressInfo;
import com.google.android.vending.expansion.downloader.DownloaderClientMarshaller;
import com.google.android.vending.expansion.downloader.DownloaderServiceMarshaller;
import com.google.android.vending.expansion.downloader.Helpers;
import com.google.android.vending.expansion.downloader.IDownloaderClient;
import com.google.android.vending.expansion.downloader.IDownloaderService;
import com.google.android.vending.expansion.downloader.IStub;
import com.seloc.techwiki.analytics.Analytics;
import com.seloc.techwiki.constants.AppConstants;
import com.seloc.techwiki.expansionfiles.DownloaderService;

public class ExpansionFilesActivity extends Activity implements IDownloaderClient {

    public static final String EXPANSION_FILES_ACTIVITY_TAG = ExpansionFilesActivity.class.getName();
    //Expansion Files Properties
    private IStub _bridge;
    private IDownloaderService _proxy;
    private ToggleButton _tbtbPauseResume;
    private ProgressBar _progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expansion_files);

        Log.d(EXPANSION_FILES_ACTIVITY_TAG, "Has main expansionFile: " + hasExpansionFile(true, AppConstants.VERSION_CODE_MAIN, AppConstants.FILESIZE_MAIN));
        Log.d(EXPANSION_FILES_ACTIVITY_TAG, "Has patch expansionFile: " + hasExpansionFile(false, AppConstants.VERSION_CODE_PATCH, AppConstants.FILESIZE_PATCH));

    /*
     *  If we've already got our expansion files there is no need to start downloading them
     */
        if (!hasExpansionFile(true, AppConstants.VERSION_CODE_MAIN, AppConstants.FILESIZE_MAIN)) {

      /*
       * Create an intent that will be started when clicking on the notification
       * Notifications expect a PendingIntent so we will wrap our Intent in a PendingIntent
       */
            Intent notificationActivity = new Intent(this, ExpansionFilesActivity.class);
            notificationActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationActivity, PendingIntent.FLAG_UPDATE_CURRENT);

            try {
                // Do we need to start any downloads?
                int needToStart = DownloaderClientMarshaller.startDownloadServiceIfRequired(this, pendingIntent, DownloaderService.class);
                if (needToStart != DownloaderClientMarshaller.NO_DOWNLOAD_REQUIRED) {
                    Log.d(EXPANSION_FILES_ACTIVITY_TAG, "Download required so setting that up");
                    // Show the user how the download is progressing
                    Analytics.logSystemEvent("Download required");
                    _bridge = DownloaderClientMarshaller.CreateStub(this, DownloaderService.class);
                    return;
                }else{
                    Log.d(EXPANSION_FILES_ACTIVITY_TAG, "Start of download not required since it must already be started.");
                    Analytics.logSystemEvent("Starting of download not required.");
                }
            } catch (PackageManager.NameNotFoundException e) {
                Analytics.logExceptionEvent(EXPANSION_FILES_ACTIVITY_TAG, "Package manager name not found when trying to start download service", e);
                Log.e(EXPANSION_FILES_ACTIVITY_TAG, e.getMessage(), e);
            }

        } else {
      /*
       * Then continue straight to our next Activity since we have already got our expansion files
       */
            Analytics.logSystemEvent("No 'manual' download required");
            Log.d(EXPANSION_FILES_ACTIVITY_TAG, "We have the expansion file so moving on to the Tech Wiki Activity");
            startActivity(new Intent(this, TechWikiActivity.class));
        }

    }

    @Override
    protected void onResume() {
        Log.d(EXPANSION_FILES_ACTIVITY_TAG, "onResume");
        if (_bridge != null) {
            Log.d(EXPANSION_FILES_ACTIVITY_TAG, "Connecting the download bridge");
            _bridge.connect(this);
        }

        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Analytics.initSession(this);
    }

    @Override
    protected void onStop() {
        Log.d(EXPANSION_FILES_ACTIVITY_TAG, "onStop");
        if (_bridge != null) {
            Log.d(EXPANSION_FILES_ACTIVITY_TAG, "Disconnecting the download bridge");
            _bridge.disconnect(this);
        }
        super.onStop();
        Analytics.stopSession(this);
    }

    private boolean hasExpansionFile(boolean mainFile, int versionCode, int fileSize) {
        String fileName = Helpers.getExpansionAPKFileName(this, mainFile, versionCode);

        Log.d(EXPANSION_FILES_ACTIVITY_TAG, "Checking if exists for file :" + fileName);

        return Helpers.doesFileExist(this, fileName, fileSize, false);
    }

    @Override
    public void onServiceConnected(Messenger m) {
        Log.d(EXPANSION_FILES_ACTIVITY_TAG, "onServiceConnected");
        _proxy = DownloaderServiceMarshaller.CreateProxy(m);
        _proxy.onClientUpdated(_bridge.getMessenger());

    /*
     *  In some cases you might want the users to download the expansion file(s) using a cellular data connection
     *  In most cases I would not recommend this since the user might be charged for the data it's using
     *  Make sure to inform/ask the user about the cellular download if you set this flag that
     *  enables the service to download when not on a WIFI network
     */
        _proxy.setDownloadFlags(IDownloaderService.FLAGS_DOWNLOAD_OVER_CELLULAR);

        _progressBar = (ProgressBar) findViewById(R.id.pbDownloadProgress);

        _tbtbPauseResume = (ToggleButton) findViewById(R.id.tbtnPauseResume);
        _tbtbPauseResume.setEnabled(true);
        _tbtbPauseResume.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton button, boolean checked) {
                if (checked) {
                    Log.d(EXPANSION_FILES_ACTIVITY_TAG, "Pausing download");
                    Analytics.logEvent("Paused download");
                    _proxy.requestPauseDownload();
                } else {
                    Log.d(EXPANSION_FILES_ACTIVITY_TAG, "Request to continue download");
                    Analytics.logEvent("Resumed download");
                    _proxy.requestContinueDownload();
                }
            }
        });
    }

    @Override
    public void onDownloadStateChanged(int newState) {
        Log.d(EXPANSION_FILES_ACTIVITY_TAG, "Download state changed : " + newState);
        if (newState == IDownloaderClient.STATE_COMPLETED) {
            Log.d(EXPANSION_FILES_ACTIVITY_TAG, "Download has completed");
            _tbtbPauseResume.setEnabled(false);

            AlertDialog.Builder adb = new AlertDialog.Builder(this);
            adb.setTitle("Download finished");
            adb.setMessage("Hurray, we have finished downloading all files needed to work offline!");
            adb.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Log.d(EXPANSION_FILES_ACTIVITY_TAG, "Moving on to Tech Wiki Activity after user clicked ok.");
                    startActivity(new Intent(ExpansionFilesActivity.this, TechWikiActivity.class));
                }
            });
            adb.show();
        }
    }

    @Override
    public void onDownloadProgress(DownloadProgressInfo progress) {
        int calculatedProgressPercentage = (int) (100 * progress.mOverallProgress / progress.mOverallTotal);
        Log.d(EXPANSION_FILES_ACTIVITY_TAG, "onDowloadProgress : " + calculatedProgressPercentage);
        _progressBar.setProgress(calculatedProgressPercentage);
    }
}
