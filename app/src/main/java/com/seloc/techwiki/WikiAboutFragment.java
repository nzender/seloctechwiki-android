package com.seloc.techwiki;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.seloc.techwiki.analytics.Analytics;

public class WikiAboutFragment extends Fragment {

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public WikiAboutFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_seloc_about, container, false);

        TextView aboutSelocText = (TextView) rootView.findViewById(R.id.selocText);
        aboutSelocText.setText(Html.fromHtml(getString(R.string.aboutSeloc)));
        aboutSelocText.setMovementMethod(LinkMovementMethod.getInstance());

        TextView techWikiText = (TextView) rootView.findViewById(R.id.techWikiText);
        techWikiText.setText(Html.fromHtml(getString(R.string.aboutTechWiki)));
        techWikiText.setMovementMethod(LinkMovementMethod.getInstance());

        TextView androidAppText = (TextView) rootView.findViewById(R.id.androidAppText);
        androidAppText.setText(Html.fromHtml(getString(R.string.androidAppText)));
        androidAppText.setMovementMethod(LinkMovementMethod.getInstance());

        this.getActivity().setTitle(R.string.app_name);

        Analytics.logEvent("About View");

        return rootView;
    }
}
