package com.seloc.techwiki.events;

import android.support.v4.app.Fragment;

public interface ContentFrameRenderer {
    public void renderContentFrame(Fragment fragment, String backStackId);
}
