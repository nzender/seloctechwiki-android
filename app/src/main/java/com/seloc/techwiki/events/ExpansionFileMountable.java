package com.seloc.techwiki.events;

public interface ExpansionFileMountable {
    public boolean mountExpansionFile();
}
