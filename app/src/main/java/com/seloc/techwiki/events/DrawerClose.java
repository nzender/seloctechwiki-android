package com.seloc.techwiki.events;

public interface DrawerClose {
    public void closeDrawer();
}
