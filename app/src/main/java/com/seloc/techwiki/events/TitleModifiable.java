package com.seloc.techwiki.events;

public interface TitleModifiable {
    public void setTitle(CharSequence title);
}
