package com.seloc.techwiki;

import android.app.ActionBar;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.storage.OnObbStateChangeListener;
import android.os.storage.StorageManager;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.SearchView;

import com.google.android.vending.expansion.downloader.Helpers;
import com.seloc.techwiki.adapters.ExpandableListAdapter;
import com.seloc.techwiki.analytics.Analytics;
import com.seloc.techwiki.constants.AppConstants;
import com.seloc.techwiki.events.ContentFrameRenderer;
import com.seloc.techwiki.events.DrawerClose;
import com.seloc.techwiki.events.ExpansionFileMountable;
import com.seloc.techwiki.events.TitleModifiable;
import com.seloc.techwiki.exceptions.ExpansionFileMountFailureException;
import com.seloc.techwiki.exceptions.IORuntimeException;
import com.seloc.techwiki.listeners.DrawerGroupItemClickListener;
import com.seloc.techwiki.listeners.DrawerItemClickListener;
import com.seloc.techwiki.model.WikiCategory;
import com.seloc.techwiki.model.WikiContent;
import com.seloc.techwiki.parser.JsonToWikiObjects;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.seloc.techwiki.constants.AppConstants.SHARED_PREF_MOUNTED_OBB_PATH;


public class TechWikiActivity extends FragmentActivity implements DrawerClose, ContentFrameRenderer, TitleModifiable, ExpansionFileMountable {
    public static final String TECHWIKI_ACTIVITY_TAG = TechWikiActivity.class.getName();
    private DrawerLayout mDrawerLayout;
    private ExpandableListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mTitle;
    private StorageManager storage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        handleIntent(getIntent());

        JsonToWikiObjects jsonToWikiObjects = new JsonToWikiObjects();
        if(jsonToWikiObjects.doesRequireGenerating()) {
            jsonToWikiObjects.generateWikiObjectsFromJson(getNavigationJson());
        }

        mTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ExpandableListView) findViewById(R.id.left_drawer);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener

        mDrawerList.setAdapter(new ExpandableListAdapter(this, WikiCategory.ITEMS));
        mDrawerList.setOnGroupClickListener(new DrawerGroupItemClickListener(this));
        mDrawerList.setOnChildClickListener(new DrawerItemClickListener(this));

        // enable ActionBar app icon to behave as action to toggle nav drawer
        final ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        // ActionBarDraw    erToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mountExpansionFile();
    }

    /**
     * @return whether it was mounted already or not
     */
    @Override
    public boolean mountExpansionFile() {
        String expansionAPKFileName = Helpers.getExpansionAPKFileName(this, true, AppConstants.VERSION_CODE_MAIN);
        final String fullObbFilePath = Helpers.generateSaveFileName(this, expansionAPKFileName);
        storage = (StorageManager) this.getSystemService(Activity.STORAGE_SERVICE);
        Log.d(TECHWIKI_ACTIVITY_TAG, "Check if obb at path " + fullObbFilePath + "already mounted: " + storage.isObbMounted(fullObbFilePath));
        File patchFile = new File(fullObbFilePath);
        final TechWikiActivity activity = this;
        if (patchFile.exists() && !storage.isObbMounted(fullObbFilePath)) {
            mountExpansionFileAtPath(fullObbFilePath, activity);
            return false;
        }
        return true;
    }

    private void mountExpansionFileAtPath(final String fullObbFilePath, final TechWikiActivity activity) {
        boolean success = storage.mountObb(fullObbFilePath, null, new OnObbStateChangeListener() {
            @Override
            public void onObbStateChange(String path, int state) {
                super.onObbStateChange(path, state);
                Log.d(TECHWIKI_ACTIVITY_TAG, path + " changed to state " + state);
                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(activity);
                switch (state) {
                    case ERROR_ALREADY_MOUNTED:
                    case ERROR_COULD_NOT_MOUNT:
                    case ERROR_COULD_NOT_UNMOUNT:
                    case ERROR_INTERNAL:
                    case ERROR_NOT_MOUNTED:
                    case ERROR_PERMISSION_DENIED:
                        throw new ExpansionFileMountFailureException("The Obb could not be mounted, state of " + state + " with path " + path);
                    case UNMOUNTED:
                        Log.e(TECHWIKI_ACTIVITY_TAG, "Obb file unmounted at path : " + path + " trying to mount again. Also removing current mount path from SharedPreferences");
                        SharedPreferences.Editor removeEditor = settings.edit();
                        removeEditor.remove(SHARED_PREF_MOUNTED_OBB_PATH);
                        removeEditor.apply();
                        removeEditor.commit();

                        mountExpansionFileAtPath(fullObbFilePath, activity);
                        break;
                    case MOUNTED:
                        String mountedObbPath = storage.getMountedObbPath(fullObbFilePath);
                        if (mountedObbPath == null) {
                            throw new NullPointerException("Could not get path to mounted OBB path");
                        }
                        Log.d(TECHWIKI_ACTIVITY_TAG, "Obb successfully mounted at path : " + mountedObbPath);
                        SharedPreferences.Editor saveEditor = settings.edit();
                        saveEditor.putString(SHARED_PREF_MOUNTED_OBB_PATH, mountedObbPath);
                        saveEditor.apply();
                        saveEditor.commit();

                        break;
                    default:
                        break;
                }
            }
        });

        if(!success){
            throw new ExpansionFileMountFailureException("Mounting of the obb at path " + fullObbFilePath + " was not successful so there is that.");
        }else{
            Log.d(TECHWIKI_ACTIVITY_TAG, "Adding mount request to queue was successful....yyaaaahhh!!!");
        }
    }

    private InputStream getNavigationJson() {
        try {
            return getAssets().open("navigation/full.json");
        } catch (IOException e) {
            throw new IORuntimeException("Error trying to read json to build navigation", e);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        if (getActionBar() != null) {
            getActionBar().setTitle(mTitle);
        }
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();

        WikiAboutFragment fragmentById = new WikiAboutFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, fragmentById)
                .commit();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            doSearch(query);
        }
    }

    private void doSearch(String query) {
        Set<WikiContent.PageItem> found = new HashSet<WikiContent.PageItem>();
        for (WikiContent.PageItem pageItem : WikiContent.ITEMS) {
            if (pageItem.title.toLowerCase().contains(query.toLowerCase())) {
                found.add(pageItem);
            }
        }

        List<WikiContent.PageItem> list = new ArrayList<WikiContent.PageItem>();
        list.addAll(found);
        ArrayList<String> foundPageIds = new ArrayList<String>();
        for (WikiContent.PageItem pageItem : list) {
            foundPageIds.add(pageItem.id);
        }


        ((SearchView) findViewById(R.id.search)).setQuery(query, false);

        HashMap<String, String> attr = new HashMap<String, String>();
        attr.put("query", query);
        attr.put("numFound", String.valueOf(foundPageIds.size()));
        Analytics.logEvent("Search Executed", attr);

        final WikiPageSearchListFragment fragmentById = new WikiPageSearchListFragment();
        Bundle args = new Bundle();
        args.putBoolean(WikiPageSearchListFragment.TWO_PANE, false);
        args.putStringArrayList(WikiPageSearchListFragment.SEARCH_RESULT_PAGE_IDS, foundPageIds);
        fragmentById.setArguments(args);
        new Handler().post(new Runnable() {
            public void run() {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, fragmentById)
                        .addToBackStack("searchResults")
                        .commit();
            }
        });
    }

    @Override
    public void renderContentFrame(Fragment fragment, String backStackId) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, fragment);

        if(backStackId != null){
            fragmentTransaction.addToBackStack(backStackId);
        }
        fragmentTransaction.commit();
    }

    @Override
    public void closeDrawer() {
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Analytics.initSession(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Analytics.stopSession(this);
    }

}