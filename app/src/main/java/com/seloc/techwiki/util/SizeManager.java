package com.seloc.techwiki.util;

import android.util.DisplayMetrics;
import android.view.WindowManager;

public class SizeManager {

    private final WindowManager windowManager;

    public SizeManager(WindowManager windowManager){
        this.windowManager = windowManager;
    }

    public float getSmallestDp() {
        DisplayMetrics metrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(metrics);
        float scaleFactor = metrics.density;
        int widthPixels = metrics.widthPixels;
        int heightPixels = metrics.heightPixels;
        float widthDp = widthPixels / scaleFactor;
        float heightDp = heightPixels / scaleFactor;
        return Math.min(widthDp, heightDp);
    }
}
