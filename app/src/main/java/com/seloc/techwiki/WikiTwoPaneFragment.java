package com.seloc.techwiki;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.seloc.techwiki.analytics.Analytics;

public class WikiTwoPaneFragment extends Fragment {

    public WikiTwoPaneFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_wikipage_twopane, container, false);

        WikiPageSubCategoryListFragment fragmentById = new WikiPageSubCategoryListFragment();
        fragmentById.setArguments(this.getArguments());
        this.getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.wikipage_list, fragmentById)
                .commit();

        Analytics.logEvent("Tablet Two Pane List View");

        return view;
    }
}
