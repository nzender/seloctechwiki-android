package com.seloc.techwiki.adapters;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.seloc.techwiki.R;
import com.seloc.techwiki.model.WikiCategory;
import com.seloc.techwiki.model.WikiSubCategory;

import java.util.List;

public class ExpandableListAdapter extends BaseExpandableListAdapter {
    private List<WikiCategory.Category> groups;
    private Context context;

    public ExpandableListAdapter(Context context, List<WikiCategory.Category> groups) {
        this.context = context;
        this.groups = groups;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return groups.get(groupPosition).subCategories.get(childPosition);
    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    // Return a child view. You can load your custom layout here.
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {
        WikiSubCategory.SubCategory subCategory = (WikiSubCategory.SubCategory) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.drawer_list_child_item, null);
        }
        TextView tv = (TextView) convertView.findViewById(R.id.child_list_item);
        tv.setText(subCategory.title);
        tv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        return convertView;
    }


    @Override
    public int getChildrenCount(int groupPosition) {
        return groups.get(groupPosition).subCategories.size();
    }


    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }


    @Override
    public int getGroupCount() {
        return groups.size();
    }


    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }


    // Return a group view. You can load your custom layout here.
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                             ViewGroup parent) {
        WikiCategory.Category group = (WikiCategory.Category) getGroup(groupPosition);
        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = infalInflater.inflate(R.layout.drawer_list_item, null);

        TextView tv = (TextView) convertView.findViewById(R.id.list_item);
        tv.setText(group.title);

        ImageView imageView = (ImageView) convertView.findViewById(R.id.group_item_icon);
        if("favorite".equals(group.id)){
            imageView.setImageResource(R.drawable.ic_action_favorite);
        }else {
            if (isExpanded) {
                imageView.setImageResource(R.drawable.ic_action_expand);
            } else {
                imageView.setImageResource(R.drawable.ic_action_collapse);

            }
        }
        return convertView;
    }


    @Override
    public boolean hasStableIds() {
        return true;
    }


    @Override
    public boolean isChildSelectable(int arg0, int arg1) {
        return true;
    }
}
