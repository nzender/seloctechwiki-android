package com.seloc.techwiki.listeners;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import com.seloc.techwiki.R;
import com.seloc.techwiki.WikiPageSearchListFragment;
import com.seloc.techwiki.analytics.Analytics;
import com.seloc.techwiki.events.ContentFrameRenderer;
import com.seloc.techwiki.events.DrawerClose;
import com.seloc.techwiki.events.TitleModifiable;
import com.seloc.techwiki.model.WikiCategory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static com.seloc.techwiki.constants.AppConstants.SHARED_PREF_FAVORITES;

public class DrawerGroupItemClickListener implements ExpandableListView.OnGroupClickListener {
    private final Activity activity;

    public DrawerGroupItemClickListener(Activity activity){
        this.activity = activity;
    }

    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        if ("favorite".equals(WikiCategory.ITEMS.get(groupPosition).id)) {
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(v.getContext().getApplicationContext());
            Set<String> favoritePages = settings.getStringSet(SHARED_PREF_FAVORITES, new HashSet<String>());
            ArrayList<String> favoritePageIdsList = new ArrayList<String>();
            favoritePageIdsList.addAll(favoritePages);


            WikiPageSearchListFragment fragmentById = new WikiPageSearchListFragment();
            Bundle args = new Bundle();
            args.putBoolean(WikiPageSearchListFragment.TWO_PANE, false);
            args.putStringArrayList(WikiPageSearchListFragment.SEARCH_RESULT_PAGE_IDS, favoritePageIdsList);
            fragmentById.setArguments(args);

            if(activity instanceof ContentFrameRenderer){
                ((ContentFrameRenderer)activity).renderContentFrame(fragmentById, "searchResults");
            }

            if(activity instanceof TitleModifiable) {
                ((TitleModifiable)activity).setTitle("Favorites");
            }

            if(activity instanceof DrawerClose){
                ((DrawerClose)activity).closeDrawer();
            }

            Analytics.logEvent("Favorites Navigation");
        }

        return false;
    }
}
