package com.seloc.techwiki.listeners;


import android.app.Activity;
import android.app.SearchManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;

import com.seloc.techwiki.R;
import com.seloc.techwiki.TechWikiActivity;
import com.seloc.techwiki.WikiPageSubCategoryListFragment;
import com.seloc.techwiki.WikiTwoPaneFragment;
import com.seloc.techwiki.events.ContentFrameRenderer;
import com.seloc.techwiki.events.DrawerClose;
import com.seloc.techwiki.model.WikiCategory;
import com.seloc.techwiki.model.WikiSubCategory;
import com.seloc.techwiki.util.SizeManager;

public class DrawerItemClickListener implements ExpandableListView.OnChildClickListener {
    private final Activity activity;
    private final SizeManager sizeManager;

    public DrawerItemClickListener(Activity activity){
        this.activity = activity;
        this.sizeManager = new SizeManager(activity.getWindowManager());
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        WikiSubCategory.SubCategory subCategory = WikiCategory.ITEMS.get(groupPosition).subCategories.get(childPosition);
        selectItem(subCategory, childPosition);
        return true;
    }

    private void selectItem(WikiSubCategory.SubCategory subCategory, int childPosition) {
        // The detail container view will be present only in the
        // large-screen layouts (res/values-large and
        // res/values-sw600dp). If this view is present, then the
        // activity should be in two-pane mode.
        if (sizeManager.getSmallestDp() > 600) {
            //Device is a 7" tablet
            WikiTwoPaneFragment fragment = new WikiTwoPaneFragment();
            Bundle args = new Bundle();
            args.putBoolean(WikiPageSubCategoryListFragment.TWO_PANE, true);
            args.putString(WikiPageSubCategoryListFragment.SELECTED_SUBCATEGORY, subCategory.id);
            fragment.setArguments(args);

            if(activity instanceof ContentFrameRenderer){
                ((ContentFrameRenderer)activity).renderContentFrame(fragment, "pane" + subCategory.id);
            }
        } else {
            WikiPageSubCategoryListFragment fragmentById = new WikiPageSubCategoryListFragment();
            Bundle args = new Bundle();
            args.putBoolean(WikiPageSubCategoryListFragment.TWO_PANE, false);
            args.putString(WikiPageSubCategoryListFragment.SELECTED_SUBCATEGORY, subCategory.id);
            fragmentById.setArguments(args);
            if(activity instanceof ContentFrameRenderer){
                ((ContentFrameRenderer)activity).renderContentFrame(fragmentById, "list" + subCategory.id);
            }
        }
        // update selected item and title, then close the drawer
        //FIXME I DO NOT UNDERSTAND THIS CODE....WHY DOES IT NOT SELECT THE CORRECT ITEM!!!!
        //mDrawerList.setItemChecked(childPosition, true);
        //mDrawerLayout.closeDrawer(mDrawerList);
        if(activity instanceof DrawerClose){
            ((DrawerClose)activity).closeDrawer();
        }
    }
}
