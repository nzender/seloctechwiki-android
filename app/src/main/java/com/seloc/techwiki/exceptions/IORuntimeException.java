package com.seloc.techwiki.exceptions;

import java.io.IOException;

public class IORuntimeException extends RuntimeException{
    public IORuntimeException(String message, IOException e){
        super(message, e);
    }
}
