package com.seloc.techwiki.exceptions;

public class ExpansionFileMountFailureException extends RuntimeException {
    public ExpansionFileMountFailureException(String message){
        super(message);
    }
}
