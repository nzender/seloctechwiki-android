package com.seloc.techwiki.exceptions;

import org.json.JSONException;

public class JSONRuntimeException extends RuntimeException{
    public JSONRuntimeException(String message, JSONException e){
        super(message, e);
    }
}
