package com.seloc.techwiki.analytics;

import android.app.Activity;

import com.flurry.android.FlurryAgent;

import org.apache.commons.lang3.Validate;
import org.apache.http.util.ExceptionUtils;

import java.util.HashMap;
import java.util.Map;

public class Analytics {
    private Analytics(){}

    private static final String FLURRY_API_KEY = "CXRQZYZPZCSQX6XYMSXZ";
    private static final String SYSTEM_EVENT_TYPE_PREFIX = "System:";
    private static final String EXCEPTION_EVENT_TYPE_PREFIX = "Exception:";

    public static void initSession(Activity activity){
        Validate.notNull(activity);

        FlurryAgent.onStartSession(activity, FLURRY_API_KEY);
    }

    public static void stopSession(Activity activity){
        Validate.notNull(activity);

        FlurryAgent.onEndSession(activity);
    }

    public static void logEvent(String name){
        Validate.notBlank(name);

        FlurryAgent.logEvent(name);
    }

    public static void logEvent(String name, Map<String, String> attributes){
        Validate.notBlank(name);

        FlurryAgent.logEvent(name, attributes);
    }

    public static void logExceptionEvent(String id, String message, Throwable throwable){
        Validate.notBlank(id);
        Validate.notBlank(message);
        Validate.notNull(throwable);

        FlurryAgent.onError(EXCEPTION_EVENT_TYPE_PREFIX + id, message, throwable);
    }

    public static void logSystemEvent(String name){
        logEvent(SYSTEM_EVENT_TYPE_PREFIX + name);
    }

    public static void logSystemEvent(String name, Map<String, String> attributes){
        logEvent(SYSTEM_EVENT_TYPE_PREFIX + name, attributes);
    }
}
