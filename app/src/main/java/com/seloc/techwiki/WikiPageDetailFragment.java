package com.seloc.techwiki;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.storage.StorageManager;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Toast;

import com.seloc.techwiki.analytics.Analytics;
import com.seloc.techwiki.events.ExpansionFileMountable;
import com.seloc.techwiki.exceptions.ExpansionFileMountFailureException;
import com.seloc.techwiki.exceptions.IORuntimeException;
import com.seloc.techwiki.model.WikiContent;
import com.seloc.techwiki.util.IOUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static com.seloc.techwiki.constants.AppConstants.SHARED_PREF_FAVORITES;
import static com.seloc.techwiki.constants.AppConstants.SHARED_PREF_MOUNTED_OBB_PATH;

/**
 * A fragment representing a single WikiPage detail screen.
 * This fragment is either contained in a ListFragment
 * in two-pane mode (on tablets) or a DetailFragment
 * on handsets.
 */
public class WikiPageDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";
    public static final String FINAL_CONTENT_PATH = "/final-content/";
    public static final String FILE_PREFIX = "file:///";
    public static final String TEXT_HTML_MIME_TYPE = "text/html";
    public static final String UTF_8 = "UTF-8";
    public static final String HTML_SUFFIX = ".html";
    public static final String ABOUT_BLANK = "about:blank";
    public static final String EMPTY = "";

    private WikiContent.PageItem mItem;
    private AlertDialog alertDialog;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public WikiPageDetailFragment() {
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem item = menu.findItem(R.id.favorite);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        item.setVisible(true);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this.getActivity().getApplicationContext());
        Set<String> favoritePages = settings.getStringSet(SHARED_PREF_FAVORITES, new HashSet<String>());
        if(favoritePages.contains(mItem.id)){
            item.setIcon(R.drawable.ic_action_favorite_dark);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.favorite) {
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this.getActivity().getApplicationContext());
            Set<String> favorites = settings.getStringSet(SHARED_PREF_FAVORITES, new HashSet<String>());

            Set<String> favoritePagesToMod = new HashSet<String>();
            for(String fav : favorites){
                favoritePagesToMod.add(fav);
            }

            HashMap<String, String> attr = new HashMap<String, String>();
            attr.put("pageId", mItem.id);
            attr.put("name", mItem.title);

            if(favoritePagesToMod.contains(mItem.id)){
                favoritePagesToMod.remove(mItem.id);
                item.setIcon(R.drawable.ic_action_favorite);
                Toast.makeText(this.getActivity(), "Favorite removed.", Toast.LENGTH_SHORT).show();
                Analytics.logEvent("User Removed Favorited Page", attr);
            }else{
                favoritePagesToMod.add(mItem.id);
                item.setIcon(R.drawable.ic_action_favorite_dark);
                Toast.makeText(this.getActivity(), "Favorite saved.", Toast.LENGTH_SHORT).show();
                Analytics.logEvent("User Favorited Page", attr);
            }

            SharedPreferences.Editor editor = settings.edit();
            editor.putStringSet(SHARED_PREF_FAVORITES, favoritePagesToMod);
            editor.apply();
            editor.commit();
        }
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            mItem = WikiContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
            setHasOptionsMenu(true);

            HashMap<String, String> attr = new HashMap<String, String>();
            attr.put("pageId", mItem.id);
            attr.put("name", mItem.title);
            Analytics.logEvent("Detail View", attr);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_wikipage_detail, container, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        builder.setTitle("Please wait...");
        builder.setMessage("Wow this is embarrassing .... We couldn't mount the filesystem. Going to keep trying....Giv'r a sec please.").setCancelable(false);
        alertDialog = builder.create();

        if (mItem != null) {
            this.getActivity().setTitle(mItem.title);

            final String originalHtmlPage = FINAL_CONTENT_PATH + mItem.id + HTML_SUFFIX;

            showPageIfStorageMounted(new Handler.Callback() {
                @Override
                public boolean handleMessage(Message msg) {
                    final String mountedObbPath = msg.getData().getString("mountedObbPath");
                    String contents = getFileContents(originalHtmlPage, mountedObbPath);

                    WebView webView = (WebView) rootView.findViewById(R.id.wikipage_detail);
                    webView.getSettings().setSupportZoom(true);
                    webView.getSettings().setUseWideViewPort(false);
                    webView.getSettings().setBuiltInZoomControls(true);
                    webView.loadDataWithBaseURL(FILE_PREFIX + mountedObbPath + FINAL_CONTENT_PATH, contents, TEXT_HTML_MIME_TYPE, UTF_8, originalHtmlPage);

                    final String finalContents = contents;
                    webView.setOnKeyListener(new View.OnKeyListener() {
                        @Override
                        public boolean onKey(View v, int keyCode, KeyEvent event) {
                            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                                WebView webView = (WebView) v;
                                if (KeyEvent.KEYCODE_BACK == keyCode && !webView.getUrl().contains(ABOUT_BLANK)) {
                                    webView.loadDataWithBaseURL(FILE_PREFIX + mountedObbPath + FINAL_CONTENT_PATH, finalContents, TEXT_HTML_MIME_TYPE, UTF_8, null);
                                    return true;
                                }
                            }
                            return false;
                        }
                    });
                    return false;
                }
            });
        }

        return rootView;
    }

    private void showPageIfStorageMounted(Handler.Callback callback) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this.getActivity().getApplicationContext());
        final String mountedObbPath = settings.getString(SHARED_PREF_MOUNTED_OBB_PATH, EMPTY);
        if(EMPTY.equals(mountedObbPath)){
            final int[] retries = {0};
            attemptToMount(retries, alertDialog, callback);
        }else{
            Bundle data = new Bundle();
            data.putString("mountedObbPath", mountedObbPath);
            Message msg = new Message();
            msg.setData(data);
            callback.handleMessage(msg);
        }
    }

    private void attemptToMount(final int[] retries, final AlertDialog alertDialog, final Handler.Callback callback) {
        final int maxRetries = 10;
        //We waited 2 seconds to load a page, show a message
        if(retries[0] == 2){
            alertDialog.show();
            Analytics.logSystemEvent("Waiting for mount dialog shown");
        }

        if(retries[0] == maxRetries){
            alertDialog.dismiss();
            RuntimeException runtimeException = new ExpansionFileMountFailureException("Android and expansion files are a complete pain in my ass. Even after multiple attempts to mount it did not happen....we are going to need to figure it out. :-/");
            Analytics.logExceptionEvent("mountFailure", "Mount did not happen", runtimeException);
            throw runtimeException;
        }
        boolean isMounted = false;//((ExpansionFileMountable)this.getActivity()).mountExpansionFile();
        if(!isMounted) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    retries[0] += 1;
                    attemptToMount(retries, alertDialog, callback);
                }
            }, 1000);
        }else{
            alertDialog.dismiss();

            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this.getActivity().getApplicationContext());
            final String mountedObbPath = settings.getString(SHARED_PREF_MOUNTED_OBB_PATH, EMPTY);

            Bundle data = new Bundle();
            data.putString("mountedObbPath", mountedObbPath);
            Message msg = new Message();
            msg.setData(data);
            callback.handleMessage(msg);

            Analytics.logSystemEvent("Had to retry mount " + retries[0] + " times before it worked");
        }
    }

    private String getFileContents(String originalHtmlPage, String mountedObbPath) {
        String contents = EMPTY;
        InputStream in = null;
        try {
            in = new BufferedInputStream(new FileInputStream(new File(mountedObbPath + originalHtmlPage)));
            contents = IOUtils.toString(in);
        } catch (IOException e) {
            throw new IORuntimeException("Issue reading file at " + mountedObbPath + originalHtmlPage, e);
        }finally{
            try {
                if(in != null) {
                    in.close();
                }
            } catch (IOException e) {
                throw new IORuntimeException("Issue closing stream", e);
            }
        }
        return contents;
    }
}
