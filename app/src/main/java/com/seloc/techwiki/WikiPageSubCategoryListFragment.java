package com.seloc.techwiki;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.seloc.techwiki.analytics.Analytics;
import com.seloc.techwiki.model.WikiContent;
import com.seloc.techwiki.model.WikiSubCategory;

import java.util.HashMap;

/**
 * A list fragment representing a list of WikiPages. This fragment
 * also supports tablet devices by allowing list items to be given an
 * 'activated' state upon selection. This helps indicate which item is
 * currently being viewed in a {@link WikiPageDetailFragment}.
 * <p/>
 */
public class WikiPageSubCategoryListFragment extends ListFragment {

    /**
     * The serialization (saved instance state) Bundle key representing the
     * activated item position. Only used on tablets.
     */
    private static final String STATE_ACTIVATED_POSITION = "activated_position";

    public static final String TWO_PANE = "twoPaneView";
    public static final String SELECTED_SUBCATEGORY = "selectedSubCategory";

    /**
     * The current activated item position. Only used on tablets.
     */
    private int mActivatedPosition = ListView.INVALID_POSITION;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public WikiPageSubCategoryListFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle listArguments = this.getArguments();
        String selectedSubCategory = listArguments.getString(SELECTED_SUBCATEGORY);
        if (listArguments != null && selectedSubCategory != null) {
            WikiSubCategory.SubCategory subCategory = WikiSubCategory.ITEM_MAP.get(selectedSubCategory);

            setListAdapter(new ArrayAdapter<WikiContent.PageItem>(
                    getActivity(),
                    android.R.layout.simple_list_item_activated_1,
                    android.R.id.text1,
                    subCategory.pages));

            HashMap<String, String> attr = new HashMap<String, String>();
            attr.put("subcategorySelected", subCategory.title);
            Analytics.logEvent("List View via Sub Category Navigation", attr);
        }else{
            Log.e(WikiPageSubCategoryListFragment.class.getName(), "No subcategory id was passed when building page list :(");
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle listArguments = this.getArguments();
        if(listArguments != null) {
            setActivateOnItemClick(listArguments.getBoolean(TWO_PANE, false));
        }

        String selectedSubCategory = listArguments.getString(SELECTED_SUBCATEGORY);
        if (listArguments != null && selectedSubCategory != null) {
            WikiSubCategory.SubCategory subCategory = WikiSubCategory.ITEM_MAP.get(selectedSubCategory);
            this.getActivity().setTitle(subCategory.title);
        }

        // Restore the previously serialized activated item position.
        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
        }
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);

        Bundle listArguments = this.getArguments();
        String selectedSubCategory = listArguments.getString(SELECTED_SUBCATEGORY);
        if (listArguments != null && selectedSubCategory != null) {
            WikiSubCategory.SubCategory subCategory = WikiSubCategory.ITEM_MAP.get(selectedSubCategory);

            Bundle arguments = new Bundle();
            arguments.putString(WikiPageDetailFragment.ARG_ITEM_ID, subCategory.pages.get(position).id);
            WikiPageDetailFragment fragment = new WikiPageDetailFragment();
            fragment.setArguments(arguments);

            if (listArguments != null && listArguments.getBoolean(TWO_PANE)) {
                // In two-pane mode, show the detail view in this activity by
                // adding or replacing the detail fragment using a
                // fragment transaction.
                this.getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.wikipage_detail_container, fragment)
                        .addToBackStack("details" + id)
                        .commit();

            } else {
                // In single-pane mode, simply start the detail activity
                // for the selected item ID.
                this.getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, fragment)
                        .addToBackStack("details" + id)
                        .commit();
            }
        }else{
            Log.e(WikiPageSubCategoryListFragment.class.getName(), "No subcategory id was passed when trying to navigate to item at position : " + position);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    /**
     * Turns on activate-on-click mode. When this mode is on, list items will be
     * given the 'activated' state when touched.
     */
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.
        getListView().setChoiceMode(activateOnItemClick
                ? ListView.CHOICE_MODE_SINGLE
                : ListView.CHOICE_MODE_NONE);
    }

    private void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            getListView().setItemChecked(mActivatedPosition, false);
        } else {
            getListView().setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }
}
