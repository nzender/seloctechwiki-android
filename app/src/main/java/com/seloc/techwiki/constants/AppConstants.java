package com.seloc.techwiki.constants;

public class AppConstants {
    private AppConstants(){};

    public static final String SHARED_PREF_FAVORITES = "sharedPrefFavorites";
    public static final String SHARED_PREF_MOUNTED_OBB_PATH = "mountedObbPath";


    // Main expansion file size
    public static final int    FILESIZE_MAIN          = 70115378;

    // Patch expansion file size
    public static final int    FILESIZE_PATCH         = 10854468;

    // Main expansion file versionCode
    public static final int    VERSION_CODE_MAIN  = 5;

    // Patch expansion file versionCode
    public static final int    VERSION_CODE_PATCH = 1;
}
