package com.seloc.techwiki.model;

import android.util.Log;

import com.seloc.techwiki.analytics.Analytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WikiSubCategory {
    public static List<SubCategory> ITEMS = new ArrayList<SubCategory>();
    public static Map<String, SubCategory> ITEM_MAP = new HashMap<String, SubCategory>();

    public static class SubCategory {
        public String id;
        public String title;
        public List<WikiContent.PageItem> pages = new ArrayList<WikiContent.PageItem>();

        public SubCategory(JSONObject pageJson) {
            try {
                this.id = pageJson.getString("id");
                this.title = pageJson.getString("name");
                ITEMS.add(this);
                ITEM_MAP.put(this.id, this);
            } catch (JSONException e) {
                String message = "Error while parsing json";
                Analytics.logExceptionEvent(WikiCategory.class.getName(), message, e);
                Log.e(SubCategory.class.getName(), message, e);
            }
        }

        public void addPage(WikiContent.PageItem page){
            if(pages == null){
                pages = new ArrayList<WikiContent.PageItem>();
            }
            pages.add(page);
        }

        @Override
        public String toString() {
            return this.title;
        }
    }
}
