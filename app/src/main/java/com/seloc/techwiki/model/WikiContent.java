package com.seloc.techwiki.model;

import android.util.Log;

import com.seloc.techwiki.analytics.Analytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WikiContent {

    public static List<PageItem> ITEMS = new ArrayList<PageItem>();
    public static Map<String, PageItem> ITEM_MAP = new HashMap<String, PageItem>();

    public static class PageItem implements Comparable<PageItem> {
        public String id;
        public String title;

        public PageItem(JSONObject pageJson) {
            try {
                this.id = pageJson.getString("pageId");
                this.title = pageJson.getString("title");
                ITEMS.add(this);
                ITEM_MAP.put(this.id, this);
            } catch (JSONException e) {
                String message = "Error while parsing json";
                Analytics.logExceptionEvent(WikiCategory.class.getName(), message, e);
                Log.e(PageItem.class.getName(), message, e);
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || !(o instanceof PageItem)) return false;

            PageItem pageItem = (PageItem) o;

            if (id != null ? !id.equals(pageItem.id) : pageItem.id != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            return id != null ? id.hashCode() : 0;
        }

        @Override
        public String toString() {
            return this.title;
        }

        @Override
        public int compareTo(PageItem another) {
            return this.title.compareTo(another.title);
        }
    }
}
