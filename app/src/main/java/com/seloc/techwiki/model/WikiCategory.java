package com.seloc.techwiki.model;

import android.util.Log;

import com.seloc.techwiki.analytics.Analytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class WikiCategory {

    public static List<Category> ITEMS = new ArrayList<Category>();

    public static class Category {
        public String id;
        public String title;
        public List<WikiSubCategory.SubCategory> subCategories = new ArrayList<WikiSubCategory.SubCategory>();

        public Category(JSONObject pageJson) {
            try {
                this.id = pageJson.getString("id");
                this.title = pageJson.getString("name");
                ITEMS.add(this);
            } catch (JSONException e) {
                String message = "Error while parsing json";
                Analytics.logExceptionEvent(WikiCategory.class.getName(), message, e);
                Log.e(Category.class.getName(), message, e);
            }
        }

        public void addSubCategory(WikiSubCategory.SubCategory subCategory){
            if(subCategories == null){
                subCategories = new ArrayList<WikiSubCategory.SubCategory>();
            }
            subCategories.add(subCategory);
        }

        @Override
        public String toString() {
            return this.title;
        }
    }
}
