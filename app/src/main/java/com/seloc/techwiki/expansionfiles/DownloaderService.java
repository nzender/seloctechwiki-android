package com.seloc.techwiki.expansionfiles;

public class DownloaderService extends com.google.android.vending.expansion.downloader.impl.DownloaderService {
    // Public key belonging to your Play Store account
    public static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvr1T2TKfgfTnpJbP7JUYn4FAq0V/MwppL9DeH9Yj+6xQgFZMm3a94QLnjC5nzKLRqqOu7GV6gTJcQQjbrgUIG/WSrw2tRO0pt11ecA0yjbwGZLnZhZxGenFvYxtX7T4y0ml3Pi7rZqN0MNv/BQJU8ERB2z5JAdC0PiYFIgxZLuQn6zEgMzyb6tw3Hrj4xbaG7ep5kL/gTC2ORXWaxUD9d3aaSNhXa9sLguhdc1a12TpmWwv/07kKtCuleX3nci7qEy4yxZxQpdsazGkcGq/iuRb7EginQ1wrbTVYUhToq9QWM1GghMIxlMOhaGdjlZcEdnZKpLfH2sD5pKXKay/VNwIDAQAB";

    public static final byte[] SALT = new byte[]{45, 54, 55, 53, 99, 53, 51, 52, 48, 54, 56, 48, 54, 99, 56, 57, 50};

    @Override
    public String getPublicKey() {
        return BASE64_PUBLIC_KEY;
    }

    @Override
    public byte[] getSALT() {
        return SALT;
    }

    @Override
    public String getAlarmReceiverClassName() {
        return DownloadBroadcastReceiver.class.getName();
    }

}
