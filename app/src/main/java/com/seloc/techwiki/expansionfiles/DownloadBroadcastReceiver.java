package com.seloc.techwiki.expansionfiles;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

import com.google.android.vending.expansion.downloader.DownloaderClientMarshaller;
import com.seloc.techwiki.analytics.Analytics;

public class DownloadBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            DownloaderClientMarshaller.startDownloadServiceIfRequired(context, intent, DownloadBroadcastReceiver.class);
        } catch (NameNotFoundException e) {
            Analytics.logExceptionEvent(DownloadBroadcastReceiver.class.getName(), "Name not found when trying to start download service", e);
            Log.e(DownloadBroadcastReceiver.class.getName(), e.getMessage(), e);
        }
    }
}
