package com.seloc.techwiki.expansionfiles;

import com.android.vending.expansion.zipfile.APEZProvider;

public class ZipUriProvider extends APEZProvider {
    @Override
    public String getAuthority() {
        return "com.seloc.techwiki.expansionfiles.ZipUriProvider";
    }
}
