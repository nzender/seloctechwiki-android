package com.seloc.techwiki.parser;

import android.content.Context;

import com.seloc.techwiki.exceptions.IORuntimeException;
import com.seloc.techwiki.exceptions.JSONRuntimeException;
import com.seloc.techwiki.model.WikiCategory;
import com.seloc.techwiki.model.WikiContent;
import com.seloc.techwiki.model.WikiSubCategory;
import com.seloc.techwiki.util.IOUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class JsonToWikiObjects {
    public void generateWikiObjectsFromJson(InputStream inputStream){
        if (doesRequireGenerating()) {
            parseJsonIntoWikiObjects(inputStream);

            addFavoriteNavigationItem();
        }
    }
    public boolean doesRequireGenerating(){
        return (WikiCategory.ITEMS.isEmpty() || WikiSubCategory.ITEMS.isEmpty() || WikiContent.ITEMS.isEmpty());
    }

    private void parseJsonIntoWikiObjects(InputStream inputStream) {
        try {
            String contents = IOUtils.toString(inputStream);
            JSONObject navigationObject = new JSONObject(contents);

            JSONArray categories = navigationObject.getJSONArray("categories");
            for (int i = 0; i < categories.length(); i++) {
                JSONObject category = categories.getJSONObject(i);
                WikiCategory.Category categoryObj = new WikiCategory.Category(category);

                JSONArray subCategories = category.getJSONArray("children");
                for (int j = 0; j < subCategories.length(); j++) {
                    JSONObject subCategory = subCategories.getJSONObject(j);
                    WikiSubCategory.SubCategory subCategoryObj = new WikiSubCategory.SubCategory(subCategory);
                    categoryObj.addSubCategory(subCategoryObj);

                    JSONArray pages = subCategory.getJSONArray("children");
                    for (int k = 0; k < pages.length(); k++) {
                        WikiContent.PageItem page = new WikiContent.PageItem(pages.getJSONObject(k));
                        subCategoryObj.addPage(page);
                    }
                }
            }


        } catch (IOException e) {
            throw new IORuntimeException("Error trying to read input stream to string", e);
        } catch (JSONException e) {
            throw new JSONRuntimeException("Error parsing json", e);
        }
    }

    private void addFavoriteNavigationItem() {
        JSONObject favoriteJson = new JSONObject();
        try {
            favoriteJson.put("id", "favorite");
            favoriteJson.put("name", "Favorite Pages");
        } catch (JSONException e) {
            throw new JSONRuntimeException("Issue trying to build favorite json", e);
        }
        new WikiCategory.Category(favoriteJson);
    }
}
